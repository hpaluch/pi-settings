###My Raspberry PI (Raspbian) settings
Here are my customized files and scripts used on Raspberry PI (Raspbian distributions). Used on Raspbian 7.1, Raspberry PI B revision.

These files are provided without any warranty.

Subdirectory "tree" contains file modified by me. 

Warning! Do not checkout directly to your root filesystem! Use some kind of workinng directory, review changes and then copy selected files.

##My favorite packages

Just cut and paste whatever needed...

	:::bash
	# Do not forget...
	sudo apt-cache update

	sudo apt-get install mlocate
	sudo apt-get install mpg123
	sudo apt-get install chromium-browser
	sudo apt-get install vim-nox
	sudo apt-get install mc
	sudo apt-get install lynx-cur

        # nice font for LXTerm
	sudo apt-get install xfonts-terminus

	# screen lock won't work without xscreensaver (or similar)
	sudo apt-get install xscreensaver-gl xscreensaver


	# for searching packages containing specific files
	sudo apt-get install apt-file

	# for cautios sysadmins - verify md5 sums of installed packages
	sudo apt-get install debsums

##Misc commands

	:::bash
	# H.264 videe player (already installed)
	omxplayer ...

	# persistent IP Tables
	apt-get install iptables-persistent
	
	# jekyll static blog generator
	sudo apt-get install ruby1.9.1-dev
	sudo gem install jekyll

	# apache2
	sudo apt-get install dnsutils
	# nslookup
	apt-get install dnsutils
	# telnet
	sudo apt-get install telnet
	

